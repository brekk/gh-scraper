# GH Scraper

## Uses fetch to get the data you need

### Install

`yarn`

### Run

`yarn start`

### Description & Choices 

For this solution, everything is in the browser.

* I'm using `create-react-app` + `emotion` for basic react and css-in-js styling.
* I'm using `f-utility` for general FP concerns (similar to `ramda` in functionality plus some niceties, also a public library I maintain)
* I'm using `ky` + `fluture` for fetch-like functionality + monadic, lazy, cancellable async concerns. (and I elected to directly use fetch + https://cors-anywhere.herokuapp.com to easily do the proxying)
* I'm using `rebass` and `rebass/grid` and `blem` for further css-in-js concerns, as well as BEM notation (`blem` is my library and I think BEM notation is especially helpful for debugging later, but relative to this being a test I only halfheartedly implemented the BEM architecture)
* It stuffs the data into localStorage so that subsequent requests are from cache.

### Issues

* I only spent a little time testing and validating functionality, relative to other job application pressures.
  + `jest` is normally my go-to testing library, but right now there's some issue with `export` if you run `jest src` directly. 
  + Somewhere in the application I'm apparently attempting to render a function, as there's a warning in the console now. However I wasn't able to track down the source of this in the timeframe I have.
  + Right now it renders errors and the README content using the same view, which is not optimal. Given more time I would split them into two separate components.
  + Styling-wise I've only done a very basic amount, with simple responsive styling.
  + Rather than use the Github API I'm scraping the content from //github.com{x} where x is something like 'lodash/lodash' => I chose to do this as the pagination behavior of the Github API seemed like something I'd have to spend a lot of time on, and relative to not doing anything with the commits other than counting them, it seemed easier to grab it from the web instead.
