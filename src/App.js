/** @jsx jsx */
import { Global, jsx, css } from "@emotion/core"
import ReactDOM from "react-dom"
import ky from "ky"
import dayjs from "dayjs"
import relativeTime from "dayjs/plugin/relativeTime"
import React from "react"
import blem from "blem"
import { withHandlers, withState } from "recompose"
import {
  isFunction,
  prop,
  map,
  pipe,
  curry,
  pathOr,
  reduce,
  propOr
} from "f-utility"
import { trace } from "xtrace"
import Future from "fluture"

import { Text, Heading } from "@rebass/emotion"
import { Flex, Box } from "@rebass/grid/emotion"

dayjs.extend(relativeTime)

const zstyle = bem => (x, y) => ({ className: y ? bem(x) : bem(x, y) })

// fetch + fluture
const grab = x =>
  new Future((bad, good) => {
    ky.get(`https://cors-anywhere.herokuapp.com/https://github.com/${x}`)
      .text()
      .catch(bad)
      .then(good)
  })

const save = curry((key, value) => {
  const derived = isFunction(value) ? value() : value
  localStorage.setItem(key, derived)
  // return last parameter so this behaves as a side effect
  return value
})

const slugify = x => x.replace("/", "-")

const smartGrab = x => {
  const stored = localStorage.getItem(slugify(x))
  if (stored) {
    return Future.of(stored)
  }
  return grab(x).map(
    pipe(
      slugify,
      save
    )(x)
  )
}

const decorate = pipe(
  withHandlers({
    showReadme: ({ setError }) => readme => setError(readme),
    hideModal: ({ setError }) => () => {
      console.log("hide modal clicked?")
      setError(false)
    },
    searchGithub: props => x => {
      return smartGrab(x)
        .map(y => {
          const parser = new DOMParser()
          const doc = parser.parseFromString(y, "text/html")
          const commits = pipe(
            sel => doc.querySelector(sel),
            c =>
              !!c
                ? pipe(
                    propOr("", "textContent"),
                    z => z.replace(",", ""),
                    parseInt
                  )(c)
                : -1
          )(".commits .num")
          const pullRequests = pipe(
            sel => doc.querySelector(sel),
            trace("uhhh"),
            c =>
              !!c
                ? pipe(
                    propOr("", "textContent"),
                    z => z.replace(",", ""),
                    parseInt
                  )
                : -1
          )(`a[href="/${x}/pulls"] .Counter`)
          const description = pipe(
            z => doc.querySelector(z),
            propOr("", "textContent"),
            z => z.trim()
          )("span[itemprop=about]")
          const readme = pipe(
            z => doc.querySelector(z),
            propOr("", "textContent")
          )(".entry-content")
          const output = {
            request: x,
            pullRequests,
            commits,
            readme,
            description,
            date: new Date()
          }
          return output
        })
        .fork(props.setError, props.addRequest)
    }
  }),
  withHandlers({
    addRequest: props => x => {
      const merged = [x].concat(props.pastRequests)
      console.log(merged, `MERGED?`)
      props.setPastRequests(merged)
    }
  }),
  withState("minorError", "setMinorError", ""),
  withState("error", "setError", ""),
  withState("searchValue", "setSearchValue", ""),
  withState("pastRequests", "setPastRequests", [])
)

const Modal = ({ children, hideModal }) =>
  ReactDOM.createPortal(
    <Box
      p={4}
      css={css`
        width: 100%;
        height: 100%;
        display: block;
        z-index: 10000;
        position: absolute;
        top: 0;
        left: 0;
        background-color: white;
        text-align: center;
        vertical-align: middle;
      `}
    >
      <Box
        as="button"
        onClick={hideModal}
        css={css`
          float: right;
          cursor: pointer;
          font-size: 2rem;
          border: 0;
          text-align: center;
          background-color: transparent;
          width: 3rem;
          height: 3rem;
          vertical-align: middle;
          &:hover {
            color: white;
            background-color: black;
          }
        `}
      >
        &times;
      </Box>
      {children}
    </Box>,
    document.getElementById("modal")
  )

const ErrorModal = pipe(
  ({ children: e, hideModal, searchValue }) => {
    let error = e.toString()
    if (error === "HTTPError: Not Found") {
      error = (
        <Box
          p={6}
          css={css`
            height: 10rem;
            vertical-align: middle;
            min-height: 10rem;
          `}
        >
          {`Unable to find the repo `}
          <a href={`//github.com/${searchValue}`}>{searchValue}</a>
          {`, are you sure it's a valid repo?`}
        </Box>
      )
    }
    return { hideModal, children: error }
  },
  Modal
)

const formatify = x => dayjs(x).fromNow() //.format(`DD-MM-YYYY - HH:mm (Z[GMT])`)

const PastRequest = ({
  request,
  commits,
  pullRequests,
  readme,
  date,
  showReadme
}) => (
  <Flex
    key={request}
    flexDirection="column"
    p={2}
    m={2}
    mb={[4, 0]}
    flexWrap="wrap"
    css={css`
      min-height: 6rem;
      @media (min-width: 40rem) {
        border: 1px solid #888;
        flex-direction: row;
      }
    `}
  >
    <Box width={[1, 1 / 3]} p={2}>
      <Heading textAlign="center" fontSize={3}>
        <a
          href={`//github.com/${request}`}
          css={css`
            text-decoration: none;
          `}
        >
          {request.replace("/", " / ")}
        </a>
      </Heading>
    </Box>
    <Flex width={[1, 2 / 3]} flexDirection="row" alignItems="baseline" p={2}>
      <Box width={[1 / 3]}>
        <Text textAlign="center" fontWeight="bold">
          Commits
        </Text>
        <Text textAlign="center">{commits}</Text>
      </Box>
      <Box width={[1 / 3]}>
        <Text textAlign="center" fontWeight="bold">
          Pull requests
        </Text>
        <Text textAlign="center">{pullRequests}</Text>
      </Box>
      <Box as="button" width={[1 / 3]} onClick={() => showReadme(readme)}>
        View Readme
      </Box>
    </Flex>
    <Box mt={2} width={1}>
      <Text textAlign="center">{`Last requested: ${formatify(date)}`}</Text>
    </Box>
  </Flex>
)

const App = ({
  bem = blem("App"),
  zz = zstyle(bem),
  pastRequests = [],
  hideModal,
  searchValue,
  setSearchValue,
  searchGithub,
  error,
  setError,
  minorError,
  setMinorError,
  showReadme
}) => (
  <>
    <Global
      styles={`
      html, body {
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 16px;
        margin: 0;
        padding: 0;
      }
    `}
    />
    {error && (
      <ErrorModal searchValue={searchValue} hideModal={hideModal}>
        {error}
      </ErrorModal>
    )}
    <Flex
      as="section"
      {...zz()}
      flexDirection="column"
      css={css`
        @media (min-width: 40rem) {
          flex-direction: row;
        }
      `}
    >
      <Flex p={2} width={[1, 1 / 4]} flexDirection="column">
        <Heading my={2}>Search GitHub</Heading>
        <Flex flexDirection="row">
          <Box
            p={2}
            width={0.7}
            as="input"
            placeholder="e.g. brekk/katsu-curry"
            type="text"
            value={searchValue}
            onChange={pipe(
              pathOr("", ["target", "value"]),
              setSearchValue
            )}
          />
          <Box
            ml={2}
            p={2}
            width={0.3}
            as="button"
            onClick={() => {
              if (~searchValue.indexOf("/")) {
                searchGithub(searchValue)
              } else {
                setMinorError(
                  `Expected a slash in the request, like: 'user/repo', please try again`
                )
              }
            }}
          >
            Search
          </Box>
        </Flex>
        {minorError && (
          <Box width={1} m={2}>
            <Text>{minorError}</Text>
          </Box>
        )}
      </Flex>
      <Flex width={[1, 3 / 4]} flexDirection="column">
        {map(
          x => (
            <PastRequest key={x.request} {...x} showReadme={showReadme} />
          ),
          pastRequests
        )}
      </Flex>
    </Flex>
  </>
)

export default decorate(App)